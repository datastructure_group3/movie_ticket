import java.util.Scanner;

public class Date {
  
    Movie movie = new Movie();

    private static String[] days = {"SAT 11 AUG","SUN 12 AUG","MON 13 AUG"};
    private static String selectday;
    private int daynum;

    public String[] getDays() {
        return days;
    }
    
    public void setSelectday(int num){
        this.selectday = days[num];
    }

    public String getSelectday(){
        return selectday;
    }
        
    public void selectDate(){
        System.out.println("-------------------------------------------------------");
        System.out.println();
        System.out.println("Plese select a date");
        System.out.println();
        System.out.println("1) "+days[0]+"\n"+"2) "+days[1]+"\n"+"3) "+days[2]);
    }

    public void inputdate(){
        Scanner kb = new Scanner(System.in);
        System.out.println("-------------------------------------------------------");
        System.out.print("Input date: ");
        daynum = kb.nextInt(); 
        if(daynum == 1 || daynum == 2 || daynum == 3 ){
            daynum = daynum-1;
            setSelectday(daynum);
        }else{
            inputdate();
        }

    }
    
    public void test(){
        System.out.println("test");

    }
    
    public static void getSelectDate(){
        // System.out.print("The date you selected is:");
        System.out.println(selectday);
    }
    }

