import java.util.Scanner;

public class Movie {
    private static int numMovies = 3; 
    public static String[] movie = {"Ophenhimer","Barbie","Meg2","-","-","-"};
    private static String[] type = {"History","Comedy","Action","-","-","-"};
    private static String[] time = {"3.10","1.30","1.40","-","-","-"};
    private static String selectmovie; //ตัวแปรนี้เอาไว้เก็บหนังที่ผู้ใช้เลือก
    private static int movienum;


    public Movie(){

    }
    public Movie(int i){
        this.selectmovie = movie[i];
    }

    public String[] getMovie(){
        return movie;
    }

    public String[] getType(){
        return type;
    }

    public String[] getTime(){
        return time;
    }

    public String getSelect(){
        return selectmovie;
    }

    public void setMovienum(){
        this.movienum = movienum-1;
    }

    public int getMovienum(){
        return movienum;
    }

    @Override
    public String toString(){
        return "movie: "+movie;
    }

    public static void inputmovie(){
        Scanner kb = new Scanner(System.in);
        System.out.print("Enter the movie number you want: ");
        movienum = kb.nextInt();
        if(movienum != 1 && movienum != 2 && movienum != 3){
            System.out.println("Please input number 1-3 !!!");
            inputmovie();
        }else{
            movienum -= 1;
            selectmovie = movie[movienum];
            System.out.println("Your chosen movie is: "+selectmovie);
            System.out.println();
            // System.out.println(movienum);
        }
    
        
    }

    public static void img(){
            System.out.println("    _________________");
            System.out.println("    |               |");
            System.out.println("    |               |");
            System.out.println("    |               |");
            System.out.println("    |      IMG      |");
            System.out.println("    |               |");
            System.out.println("    |               |");
            System.out.println("    |               |");
            System.out.println("    |_______________|");
            System.out.println();
    }

    static int numMovie = 3;
    public static void showmovie() {
        
        for (int i = 0; i < numMovie; i++) {
            if(movie[i] == "-"){
                break;
            }else{
                System.out.println("-------------------------------------------------------");
                System.out.println("    _________________");
                System.out.println("    |               |");
                System.out.println("    |               |");
                System.out.println("    |               |  Name: "+movie[i]);
                System.out.println("    |      IMG      |  Type: "+type[i]);
                System.out.println("    |               |");
                System.out.println("    |               |  Time: "+time[i]+" Hr.");
                System.out.println("    |               |");
                System.out.println("    |_______________|");
                System.out.println();
            }
            
            
        }
        System.out.println("-------------------------------------------------------");
        System.out.println();
    }

    public static void getSelectMovie(){
        System.out.println(selectmovie);
    }

    public static void addMovie(){

        Scanner sc = new Scanner(System.in);
        System.out.print("Number add Movie: ");
        numMovies = sc.nextInt();
        for(int i = 0; i<numMovies; i++){
            System.out.print("Movie name "+(i+1)+" : ");
            movie[numMovie] = sc.next();
            System.out.print("Type: ");
            type[numMovie] = sc.next();
            System.out.print("Time: ");
            time[numMovie] = sc.next();
            numMovie++;
            
        }
        showmovie();
        
    }
    
    // public static void main(String[] args) {
    //     showmovie();
    //     inputmovie();
    //     addMovie();


    // }


    //delete movie
    private void deleteMovie(int deletedMovie){
        Movie.movie[deletedMovie-1] = "-";
        Movie.type[deletedMovie-1]  = "-";
        Movie.time[deletedMovie-1]  = "-";     
    }

    public void deletedAndPrintMovie() {
        Scanner sc = new Scanner(System.in);
        System.out.println("select number of deleted movie");
        int deletedMovie = sc.nextInt();

        Movie movie = new Movie();
        movie.deleteMovie(deletedMovie);
        String[] movieArray = movie.getMovie();
        String[] typeArray = movie.getType();
        String[] timeArray = movie.getTime();

        for (int i = 0; i < Movie.movie.length; i++) {
            System.out.println("Movie: " + (i + 1));
            System.out.println("Movie: " + movieArray[i]);
            System.out.println("Type: " + typeArray[i]);
            System.out.println("Time: " + timeArray[i]);
            System.out.println();
        }
    }
    

}
