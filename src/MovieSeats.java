import java.util.Scanner;

public class MovieSeats {
    String alphaBet = "ABCDEF";
    String alphaNum = "123456789";
    String[] Arr = new String[alphaBet.length() * alphaNum.length()];
    static String[] select2;
    static int num;
    String seat;
    static String[] selectSeat;

    public MovieSeats() {

    }

    public MovieSeats(String Arr[]) {
        this.Arr = Arr;
    }

    public void setselectSeat(String[] a) {
        this.selectSeat = a;
    }

    public void set_original_seats() {
        for (int i = 0; i < alphaBet.length(); i++) {
            for (int j = 0; j < alphaNum.length(); j++) {
                int index = i * alphaNum.length() + j;
                Arr[index] = String.valueOf(alphaBet.charAt(i)) + alphaNum.charAt(j);
            }
        }

    }

    public void showseats() {
        System.out.println("-------------------------------------------------------");

        System.out.println();
        System.out.println("                   Screen");
        System.out.println("==============================================");
        System.out.println();
        System.out.println();
        System.out.print("| ");
        int col = 0;
        for (int i = 0; i < Arr.length; i++) {
            if (col == 9) {
                System.out.println();
                System.out.println();
                System.out.print("| ");
                col = 0;
                System.out.print(Arr[i] + " | ");
                col++;
            } else {
                System.out.print(Arr[i] + " | ");
                col++;
            }
        }
        System.out.println();
        System.out.println();
        System.out.println("-------------------------------------------------------");

    }

    public MovieSeats(int num) {
        this.num = num;
    }

    public static int getNum() {
        return num;
    }

    public static void numOfTicket() {
        num = 0;
        Scanner kb = new Scanner(System.in);
        priceSeat();
        System.out.print("How many movie tickets do you want to buy?: ");
        num = kb.nextInt();
        select2 = new String[num];
    }

    public void inputseat() {
        Scanner kb = new Scanner(System.in);
        int seatnum = 0;
        System.out.println();
        for (int i = 0; i < num; i++) {
            System.out.print("Please input seat ");
            System.out.print(i + 1 + " : ");
            seat = kb.next().toUpperCase();
            boolean check = false;
            for (int n2 = 0; n2 < Arr.length; n2++) {
                if (seat.equals(Arr[n2])) {
                    check = true;
                    break;
                }
            }
            if (check == false) {
                System.out.println("Can't find seat " + seat + " it may be because this seat");
                System.out.println("is not available or has already been reserved");
                set_original_seats();
                seat = "";
                inputseat();
            } else if (check == true) {
                String[] seatArray = seat.split(" ");
                for (String seatPart : seatArray) {
                    for (int j = 0; j < Arr.length; j++) {
                        if (Arr[j].equals(seatPart)) {
                            select2[seatnum] = Arr[j];
                            Arr[j] = "X ";
                            seatnum++;
                            break;
                        }
                    }
                    showseats();
                }
            }

        }
        // System.out.println("finish");
        setselectSeat(select2);
    }

    // if (Arr[j].equals(seatPart)) {
    // select2[x] = Arr[j];
    // Arr[j] = "X ";
    // x++;
    // break;
    // }

    public static void showseatselected() {
        System.out.print("Seat: ");
        for (String seat : selectSeat) {
            System.out.print(seat + " ");
        }
        System.out.println();
    }

    public static void priceSeat() {
        System.out.println();
        System.out.println("                 100 Baht / Seat");
        System.out.println();
    }

    public static void showlSelect() {
        // showseatselected();
    }
}