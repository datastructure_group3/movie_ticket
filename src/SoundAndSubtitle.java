import java.util.Scanner;

public class SoundAndSubtitle {

    static String SoundSub[] = {"Thai / No Sub", "Eng / Sub Thai", "Original Sound / Sub Thai", "Original Sound / Sub Eng"};
    static String selectSub;

    public SoundAndSubtitle(){

    }

    public static void showSoundSub(){
        System.out.println("-------------------------------------------------------");
        System.out.println();
        System.out.println("Choose sound and subtitles");
        System.out.println("1) Thai / No Sub");
        System.out.println("2) Eng / Sub Thai");
        System.out.println("3) Original Sound / Sub Thai");
        System.out.println("4) Original Sound / Sub Eng");
        System.out.println();

    }

    public static void inputSoundSub(){
        Scanner sc = new Scanner(System.in);
        System.out.println("-------------------------------------------------------");
        System.out.print("Please enter number: ");
        int sosub = sc.nextInt();
        if(sosub == 1){
            selectSub = SoundSub[0];
        }else if(sosub == 2){
            selectSub = SoundSub[1];
        }else if(sosub == 3){
            selectSub = SoundSub[2];
        }else if(sosub == 4){
            selectSub = SoundSub[3];
        }else{
            System.out.println("Please enter number 1-4 !!!");
            inputSoundSub();
        }
    }

    public static void SelectSub(){
        System.out.println("Your choice of sounds and subtitles: "+selectSub);
    }

    public static void showSelectSub(){
        System.out.println(selectSub);
    }
    
    
}
